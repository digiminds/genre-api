﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using GenreApp.Models;
using Insight.Database;

namespace GenreApp.DataAccess
{
    public class AppRepository
    {
        #region Fields

        private static AppRepository _instance;
        private IDbConnection _connection;
        private string _connectionString;
        private static object _lock = new object();

        #endregion

        #region Properties

        public static AppRepository Instance
        {
            get
            {
                lock (_lock)
                {
                    if (_instance == null)
                    {
                        _instance = new AppRepository("DefaultConnection");
                    }
                }
                return _instance;
            }
        }

        public IDbConnection DB
        {
            get
            {
                lock (_lock)
                {
                    if (_connection == null)
                    {
                        _connection = new SqlConnection(GetConnectionString(_connectionString));
                    }
                }
                return _connection;
            }
        }

        #endregion

        #region Constructor

        public AppRepository(string ConnectionString)
        {
            _connectionString = ConnectionString;
        }

        #endregion

        #region Public Methods

        public int InsertApplicant(Applicant applicant)
        {
            return DB.Insert(GetProcedureName(Constants.ApplicantTable, Constants.Insert), applicant)?.Code ?? 0;
        }

        public int UpdateApplicant(Applicant applicant)
        {
            return DB.Execute(GetProcedureName(Constants.ApplicantTable, Constants.Update), applicant);
        }

        public int UpsertAppsRating(IEnumerable<AppRating> appsRating)
        {
            return DB.Execute(GetProcedureName(Constants.AppRatingTable, Constants.UpsertMany), appsRating);
        }

        #endregion

        #region Private Methods

        private string GetConnectionString(string cs)
        {
            string result = cs;
            var connectionString = ConfigurationManager.ConnectionStrings[cs];
            if (connectionString != null)
            {
                result = connectionString.ConnectionString;
            }
            if (!string.IsNullOrEmpty(result))
            {
                return result;
            }
            else
            {
                throw new InvalidOperationException(String.Format("Invalid Connection SSstring {0}", cs));
            }
        }

        private string GetProcedureName(string tableName, string actionName)
        {
            return string.Format("usp_{0}_{1}", tableName, actionName);
        }

        #endregion
    }
}