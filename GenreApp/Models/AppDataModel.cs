﻿using System.Collections.Generic;

namespace GenreApp.Models
{
    public class AppDataModel
    {
        public int Code { get; set; }
        public ApplicantInfo ApplicantInfo { get; set; }
        public IEnumerable<ApplicationInfo> Applications { get; set; }
    }

    public class ApplicantInfo
    {
        public string Gender { get; set; }
        public int Age { get; set; }
        public bool IsASmoker { get; set; }
        public int Height { get; set; }
        public int Weight { get; set; }
        public int HealthStatus { get; set; }
        public int NowAge { get; set; }
        public string NowBmi { get; set; }
        public string MedicalUnderwritingExperience { get; set; }
    }

    public class ApplicationInfo
    {
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool Rated { get; set; }
        public ApplicationRating RatingInformation { get; set; }
    }

    public class ApplicationRating
    {
        public int Creativity { get; set; }
        public int EaseOfUse { get; set; }
        public int LifeStyleProposition { get; set; }
        public int HealthStatusApplicability { get; set; }
        public int FututerSuccessOpportunities { get; set; }
        public int Score { get; set; }
    }
}