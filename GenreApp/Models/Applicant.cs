﻿namespace GenreApp.Models
{
    public class Applicant
    {
        public int Code { get; set; }
        public string Gender { get; set; }
        public int Age { get; set; }
        public bool IsASmoker { get; set; }
        public int Height { get; set; }
        public int Weight { get; set; }
        public int HealthStatus { get; set; }
        public int NowAge { get; set; }
        public string NowBmi { get; set; }
        public string MedicalUnderwritingExperience { get; set; }
    }
}