﻿namespace GenreApp.Models
{
    public class AppRating
    {
        public int ApplicantCode { get; set; }
        public string AppCode { get; set; }
        public string AppTitle { get; set; }
        public int Creativity { get; set; }
        public int EaseOfUse { get; set; }
        public int LifeStyleProposition { get; set; }
        public int HealthStatusApplicability { get; set; }
        public int FututerSuccessOpportunities { get; set; }
        public int Score { get; set; }
    }
}