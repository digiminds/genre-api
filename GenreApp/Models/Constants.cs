﻿namespace GenreApp.Models
{
    public class Constants
    {
        public const string ApplicantTable = "Applicant";
        public const string AppRatingTable = "AppRating";

        public const string Insert = "Insert";
        public const string Update = "Update";
        public const string UpsertMany = "UpsertMany";
    }
}