﻿namespace GenreApp.Models
{
    public class Application
    {
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}