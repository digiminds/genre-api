﻿using System;
using System.Web.Http;
using GenreApp.Business;
using GenreApp.Models;

namespace GenreApp.Controllers
{
    public class AppController : ApiController
    {
        #region Fields

        private Lazy<AppManager> _appManager;

        #endregion

        #region Properties

        public AppManager Manager => _appManager.Value;

        #endregion

        #region Constructor

        public AppController()
        {
            _appManager = new Lazy<AppManager>();
        }

        #endregion

        #region Public

        [HttpGet]
        public int GenerateApplicantCode()
        {
            return Manager.GenerateApplicantCode();
        }

        [HttpPost]
        public void SaveData(AppDataModel model)
        {
            if (model == null)
                throw new ArgumentNullException();
            if (model.Code == 0)
                throw new ArgumentException();

            Manager.SaveAppData(model);
        }

        #endregion
    }
}
