﻿using System.Web.Http;
using Insight.Database;

namespace GenreApp
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            SqlInsightDbProvider.RegisterProvider();
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
