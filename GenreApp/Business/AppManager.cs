﻿using System.Collections.Generic;
using System.Linq;
using GenreApp.DataAccess;
using GenreApp.Models;

namespace GenreApp.Business
{
    public class AppManager
    {
        #region Public Methods

        public int GenerateApplicantCode()
        {
            return AppRepository.Instance.InsertApplicant(new Applicant());
        }

        public void SaveAppData(AppDataModel model)
        {
            var applicant = MapToApplicant(model);
            if (applicant != null)
            {
                AppRepository.Instance.UpdateApplicant(applicant);
            }

            var appsRating = MapToAppsRating(model);
            if (appsRating != null && appsRating.Any())
            {
                AppRepository.Instance.UpsertAppsRating(appsRating);
            }
        }

        #endregion

        #region Private

        private Applicant MapToApplicant(AppDataModel model)
        {
            if (model?.ApplicantInfo == null) return null;

            return new Applicant
            {
                Code = model.Code,
                Age = model.ApplicantInfo.Age,
                Gender = model.ApplicantInfo.Gender,
                HealthStatus = model.ApplicantInfo.HealthStatus,
                Height = model.ApplicantInfo.Height,
                IsASmoker = model.ApplicantInfo.IsASmoker,
                MedicalUnderwritingExperience = model.ApplicantInfo.MedicalUnderwritingExperience,
                NowAge = model.ApplicantInfo.NowAge,
                NowBmi = model.ApplicantInfo.NowBmi,
                Weight = model.ApplicantInfo.Weight
            };
        }

        private IEnumerable<AppRating> MapToAppsRating(AppDataModel model)
        {
            if (model?.Applications == null) return null;
            if (!model.Applications.Any()) return null;

            var appsRating = new List<AppRating>();
            foreach (var application in model.Applications)
            {
                if (application.Rated)
                {
                    var rating = MapToAppRating(model.Code, application);
                    if (rating != null)
                    {
                        appsRating.Add(rating);
                    }
                }
            }

            return appsRating;
        }

        private AppRating MapToAppRating(int applicantCode, ApplicationInfo info)
        {
            if (info?.RatingInformation == null) return null;

            return new AppRating
            {
                ApplicantCode = applicantCode,
                AppCode = info.Code,
                AppTitle = info.Title,
                Creativity = info.RatingInformation.Creativity,
                EaseOfUse = info.RatingInformation.EaseOfUse,
                FututerSuccessOpportunities = info.RatingInformation.FututerSuccessOpportunities,
                HealthStatusApplicability = info.RatingInformation.HealthStatusApplicability,
                LifeStyleProposition = info.RatingInformation.LifeStyleProposition,
                Score = info.RatingInformation.Score
            };
        }
        
        #endregion
    }
}