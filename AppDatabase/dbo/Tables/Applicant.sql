﻿CREATE TABLE [dbo].[Applicant] (
    [Code]                          INT           IDENTITY (1, 1) NOT NULL,
    [Gender]                        NVARCHAR (10) NULL,
    [Age]                           INT           NULL,
    [IsASmoker]                     BIT           NULL,
    [Height]                        NUMERIC (18)  NULL,
    [Weight]                        NUMERIC (18)  NULL,
    [HealthStatus]                  NUMERIC (18)  NULL,
    [NowAge]                        NUMERIC (18)  NULL,
    [NowBmi]                        NVARCHAR (50) NULL,
    [MedicalUnderwritingExperience] NVARCHAR (50) NULL,
    CONSTRAINT [PK_Applicant] PRIMARY KEY CLUSTERED ([Code] ASC)
);

