﻿CREATE TYPE [dbo].[AppRatingTable] AS TABLE (
    [ApplicantCode]               INT            NOT NULL,
    [AppCode]                     NVARCHAR (50)  NOT NULL,
    [AppTitle]                    NVARCHAR (100) NULL,
    [Creativity]                  INT            NULL,
    [EaseOfUse]                   INT            NULL,
    [LifeStyleProposition]        INT            NULL,
    [HealthStatusApplicability]   INT            NULL,
    [FututerSuccessOpportunities] INT            NULL,
    [Score]                       INT            NULL);

