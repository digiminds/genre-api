﻿CREATE PROCEDURE [dbo].[USP_Applicant_Update]
	@Code int,
	@Gender nvarchar(10) = null,
	@Age int = 0,
	@IsASmoker bit = 0,
	@Height numeric(18, 0) = 0,
	@Weight numeric(18, 0) = 0,
	@HealthStatus numeric(18, 0) = 0,
	@NowAge numeric(18, 0) = 0,
	@NowBmi nvarchar(50) = null,
	@MedicalUnderwritingExperience nvarchar(50) = null
AS
BEGIN
	UPDATE [Applicant] SET
		[Gender] = @Gender
		,[Age] = @Age
		,[IsASmoker] = @IsASmoker
		,[Height] = @Height
		,[Weight] = @Weight
		,[HealthStatus] = @HealthStatus
		,[NowAge] = @NowAge
		,[NowBmi] = @NowBmi
		,[MedicalUnderwritingExperience] = @MedicalUnderwritingExperience
	WHERE [Code] = @Code
END