﻿CREATE PROCEDURE USP_Applicant_Insert
	@Gender nvarchar(10) = null,
	@Age int = 0,
	@IsASmoker bit = 0,
	@Height numeric(18, 0) = 0,
	@Weight numeric(18, 0) = 0,
	@HealthStatus numeric(18, 0) = 0,
	@NowAge numeric(18, 0) = 0,
	@NowBmi nvarchar(50) = null,
	@MedicalUnderwritingExperience nvarchar(50) = null
AS
BEGIN
	INSERT INTO [Applicant]
	(
		[Gender]
		,[Age]
		,[IsASmoker]
		,[Height]
		,[Weight]
		,[HealthStatus]
		,[NowAge]
		,[NowBmi]
		,[MedicalUnderwritingExperience]
	)
	OUTPUT Inserted.Code
	VALUES
	(
		@Gender
		,@Age
		,@IsASmoker
		,@Height
		,@Weight
		,@HealthStatus
		,@NowAge
		,@NowBmi
		,@MedicalUnderwritingExperience
	)
END