﻿CREATE PROCEDURE [dbo].[USP_AppRating_UpsertMany]
(
	@AppRating [dbo].[AppRatingTable] READONLY
)
AS
MERGE INTO [dbo].[AppRating] AS t
USING (SELECT *, [_insight_rownumber] = ROW_NUMBER() OVER (ORDER BY (SELECT 1)) FROM @AppRating) AS s
ON
(
	t.[ApplicantCode] = s.[ApplicantCode] AND t.[AppCode] = s.[AppCode]
)
WHEN MATCHED THEN UPDATE SET
	t.[AppTitle] = s.[AppTitle],
	t.[Creativity] = s.[Creativity],
	t.[EaseOfUse] = s.[EaseOfUse],
	t.[LifeStyleProposition] = s.[LifeStyleProposition],
	t.[HealthStatusApplicability] = s.[HealthStatusApplicability],
	t.[FututerSuccessOpportunities] = s.[FututerSuccessOpportunities],
	t.[Score] = s.[Score]
WHEN NOT MATCHED BY TARGET THEN INSERT
(
	[ApplicantCode],
	[AppCode],
	[AppTitle],
	[Creativity],
	[EaseOfUse],
	[LifeStyleProposition],
	[HealthStatusApplicability],
	[FututerSuccessOpportunities],
	[Score]
)
VALUES
(
	s.[ApplicantCode],
	s.[AppCode],
	s.[AppTitle],
	s.[Creativity],
	s.[EaseOfUse],
	s.[LifeStyleProposition],
	s.[HealthStatusApplicability],
	s.[FututerSuccessOpportunities],
	s.[Score]
)
;